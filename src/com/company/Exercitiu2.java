package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Exercitiu2 extends JFrame {
    JTextField tf1, tf2;
    JButton button;

    Exercitiu2(){
        this.setLayout(null);
        this.setSize(400,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        tf1 = new JTextField();
        tf1.setBounds(10,10,250,80);
        this.add(tf1);

        tf2 = new JTextField();
        tf2.setBounds(10,100,250,100);
        this.add(tf2);

        button = new JButton("Switch");
        button.setBounds(100,300,100,80);
        button.addActionListener(new ButtonFunction());
        this.add(button);

        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Exercitiu2();
    }

    class ButtonFunction implements ActionListener {
        public void actionPerformed(ActionEvent e){
            String t1 = tf1.getText();
            tf2.setText(t1);
            tf1.setText("");
        }
    }
}
